
class Game {
  /* */
  north = 0;
  east = 1
  south = 2;
  west = 3;

  /* Definition of bloc type   */
  open = 1000;
  arrival = 1001;
  end = 1002;

  bloc = 2000;
  unbreakable = 2001;
  bloc_limit = 2002;

  // door can be open directly
  door_ns = 2005;
  door_ew = 2006;

  // gate need something to be open
  gate_ns = 2007;
  gate_ew = 2008;

  trap = 2010;

  getMoveFromDirection(direction) {
    switch(direction) {
    case this.north:
      return new OrthoVector(0, 1);
    case this.east:
      return new OrthoVector(1, 0);
    case this.south:
      return new OrthoVector(0, -1);
    case this.west:
      return new OrthoVector(-1, 0);
    }
    throw new Error ('Bad direction receive ' + direction);
  }

  getOrthogonalMoveFromDirection(direction) {
    switch(direction) {
    case this.north:
      return new OrthoVector(1, 0);
    case this.east:
      return new OrthoVector(0, -1);
    case this.south:
      return new OrthoVector(-1, 0);
    case this.west:
      return new OrthoVector(0, 1);;
    }
    throw new Error ('Bad direction receive ' + direction);
  }

  getRandomOrientation() {
    const r = Math.random();
    if (r < 0.25) return this.north;
    if (r < 0.50) return this.east;
    if (r < 0.75) return this.south;
    return this.west;
  }

  getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive
  }

}

// Because static fields do not work with firefox :-(
Game = new Game();

class Space {
  grid = undefined;

  min_x = undefined;
  max_x = undefined;
  min_y = undefined;
  max_y = undefined;

  constructor(blocs=[], value = undefined) {
    this.grid = new Grid();

    if (blocs.length>0) {
      this.min_x = this.max_x = blocs[0].x;
      this.min_y = this.max_y = blocs[0].y;

      for (let i = 0; i < blocs.length; i++) {
        this.grid.setB(blocs[i],value);
        let s = new Space;
        s.setMinMaxPoint(blocs[i].x, blocs[i].y);
        this.updateMinMax(s);
      }
    }
  }

  setMinMaxPoint(x,y) {
    this.min_x = this.max_x = x;
    this.min_y = this.max_y = y;
  }

  updateMinMax(space) {
    if (space.max_x == undefined ||
        space.max_y == undefined ||
        space.min_x == undefined ||
        space.min_y == undefined
       )  {
      throw new Error ('Space with without min and max are not allowed');
    }
    this.max_x = this.max_x == undefined ? space.max_x : Math.max(this.max_x, space.max_x);
    this.max_y = this.max_y == undefined ? space.max_y : Math.max(this.max_y, space.max_y);
    this.min_x = this.min_x == undefined ? space.min_x : Math.min(this.min_x, space.min_x);
    this.min_y = this.min_y == undefined ? space.min_y : Math.min(this.min_y, space.min_y);
  }

  /**
   * Return an array of constants
   */
  export() {
    const arr = [];

    let row = 0;
    for (let y = this.min_y; y <= this.max_y; y++) {
      arr[row] = [];
      let col = 0;
      for (let x = this.min_x; x <= this.max_x; x++) {
        let here = this.grid.get(x,y);
        arr[row][col] = (here == undefined) ? Game.bloc : here;
        col++;
      }
      row++;
    }
    return arr;
  }

  toString() {
    let output = `(${this.min_x},${this.min_y}) => (${this.max_x},${this.max_y})\n`;
    let row = 0;
    for (let y = this.min_y; y <= this.max_y; y++) {
      let col = 0;
      for (let x = this.min_x; x <= this.max_x; x++) {
        let here = this.grid.get(x,y);
        let c = '';
        switch (here) {
        case Game.open:
          c = ' ';
          break;
        case Game.arrival:
          c = 'A';
          break;
        case Game.end:
          c = 'E';
          break;
        case Game.unbreakable:
          c = '@';
          break;
        case Game.door_ew:
        case Game.gate_ew:
          c = '-';
          break;
        case Game.door_ns:
        case Game.gate_ns:
          c = '|';
          break;
        case Game.trap:
          c = 'X';
          break;
        case Game.bloc_limit:
        case Game.bloc:
          c = '#';
          break;
        default:
          c = 'U';
        }
        output += c;
        col++;
      }
      row++;
      output +=  ` ${y}\n`;
    }
    return output;
  }

}

class Corridor extends Space {

  // It should be open somewhere
  constructor(begin, orientation, size) {
    super();
    let step_x = 0, step_y = 0, ortho = [0,0];
    this.min_x = this.max_x = begin.x;
    this.min_y = this.max_y = begin.y;
    switch (orientation) {
    case Game.north:
      step_y = 1;
      ortho = [1,0];
      this.max_y += (size);
      this.max_x += +1;
      this.min_x += -1;
      this.min_y += -1;
      break;
    case Game.east:
      step_x = 1;
      ortho = [0,1];
      this.max_x += (size);
      this.max_y += 1;
      this.min_x -= 1;
      this.min_y -= 1;
      break;
    case Game.south:
      step_y = -1;
      ortho = [1,0];
      this.min_y -= (size);
      this.min_x -= 1;
      this.max_x += 1;
      this.max_y += 1;
      break;
    default:
      step_x = -1;
      ortho = [0,1];
      this.min_x -= (size);
      this.min_y -= 1;
      this.max_x += 1;
      this.max_y += 1;
      break;
    }
    this.grid.set(begin.x + -1*step_x, begin.y + -1*step_y, Game.bloc_limit );
    this.grid.set(begin.x + -1*step_x + ortho[0], begin.y + -1*step_y + ortho[1], Game.bloc_limit );
    this.grid.set(begin.x + -1*step_x - ortho[0], begin.y + -1*step_y - ortho[1], Game.bloc_limit );
    for (let i = 0; i < size; i++  ) {
      this.grid.set(begin.x + i*step_x, begin.y + i*step_y, Game.open );
      this.grid.set(begin.x + i*step_x + ortho[0], begin.y + i*step_y - ortho[1], Game.bloc_limit );
      this.grid.set(begin.x + i*step_x - ortho[0], begin.y + i*step_y + ortho[1], Game.bloc_limit );
    }
    this.grid.set(begin.x + (size+0)*step_x, begin.y + (size+0)*step_y, Game.bloc_limit );
    this.grid.set(begin.x + (size+0)*step_x + ortho[0], begin.y + (size+0)*step_y + ortho[1], Game.bloc_limit );
    this.grid.set(begin.x + (size+0)*step_x - ortho[0], begin.y + (size+0)*step_y - ortho[1], Game.bloc_limit );
  }
}

class Room extends Space {
  // bloc_1 and bloc_2 define the inside of the room
  constructor(bloc_1, bloc_2) {
    super();
    const x_min = Math.min(bloc_1.x, bloc_2.x);
    const y_min = Math.min(bloc_1.y, bloc_2.y);
    const x_max = Math.max(bloc_1.x, bloc_2.x);
    const y_max = Math.max(bloc_1.y, bloc_2.y);

    this.min_x = x_min - 1;
    this.min_y = y_min - 1;
    this.max_x = x_max + 1;
    this.max_y = y_max + 1;

    // outside
    for (let x = this.min_x+1 ; x < this.max_x ; x++ ) {
      this.grid.set(x, this.min_y, Game.bloc_limit);
      this.grid.set(x, this.max_y, Game.bloc_limit);
    }
    for (let y = this.min_y + 1; y < this.max_y ; y++ ) {
      this.grid.set(this.min_x, y, Game.bloc_limit);
      this.grid.set(this.max_x, y, Game.bloc_limit);
    }

    // inside
    for (let x = x_min ; x <= x_max ; x++ ) {
      for (let y = y_min; y <= y_max ; y++ ) {
        this.grid.set(x, y, Game.open);
      }
    }
  }

  static getRoomFromSide(side, direction, size) {
    const b1 = new Bloc(side.min_x, side.min_y);
    const b2 = new Bloc(side.max_x, side.max_y);
    const b3 = b2.add(Game.getMoveFromDirection(direction), size);
    return new Room(b1, b3);
  }
}

class Blocking {

}

class Door extends Blocking {

}

class Gate extends Blocking {

}

class Grid {
  arr = [];

  computeUlamSpiralNumber(x,y) {
    const l = 2 * Math.max(Math.abs(x), Math.abs(y));
    const d = (y > x) ? (l * 3 + x + y) : (l - x - y);
    return (l - 1) ** 2 + d;
  }

  setB(bloc, value) {
    this.set(bloc.x, bloc.y, value);
  }

  set(x,y, value) {
    this.arr[this.computeUlamSpiralNumber(x,y)] = [value, x, y];
  }

  getB(bloc) {
    return this.get(bloc.x, bloc.y);
  }

  get(x,y) {
    const a = this.arr[this.computeUlamSpiralNumber(x,y)];
    return a != undefined ? a[0] : undefined;
  }
}

class Bloc {
  x = undefined;
  y = undefined;

  constructor(x,y) {
    this.x = x;
    this.y = y;
  }

  add(move, time = 1) {
    return new Bloc(this.x + time*move.x, this.y + time*move.y);
  }

  toString() {
    return `Bloc[${this.x}, ${this.y}]`;
  }
}

class OrthoVector extends Bloc {
  scale(s) {
    this.x = s * this.x;
    this.y = s * this.y;
  }

  turn() {
    return new OrthoVector(this.y, -this.x);
  }
}

class LevelConstructor {
  max_distance = 100;
  min_room_pro_step = 2;
  max_room_pro_step = 4;
  min_size_room = 4;
  max_size_room = 6;
  min_size_coridor = 3;
  max_size_coridor = 6;
  max_tries = 100;

  build(difficulty_level) {
    const level = new Level(difficulty_level);
    const origin = new Bloc(256,256);
    let nb_room = 0;
    let r = false; // can bool or []

    // Build level 0
    const direction = Game.getRandomOrientation();
    const corridor_size = Game.getRandomIntInclusive(this.min_size_coridor, this.max_size_coridor);
    const corridor = new Corridor(origin, direction, corridor_size);
    level.addSpace(corridor);

    // Set arrival and end
    level.grid.setB(origin,  Game.arrival);
    level.arrival = origin;
    let end = origin.add(Game.getMoveFromDirection(direction), corridor_size-1);
    let before_end = origin.add(Game.getMoveFromDirection(direction), corridor_size-2);
    level.grid.setB(end, Game.end);

    // Begin loop on complexity
    for (let step = 0; step < level.level; step ++) {
      let nb_room = Game.getRandomIntInclusive(this.min_room_pro_step, this.max_room_pro_step);
      for (let room = 0; room < nb_room; room++ ) {
        this.addRoom(level,
                     Game.getRandomIntInclusive(this.min_size_room, this.max_size_room),
                     Game.getRandomIntInclusive(this.min_size_room, this.max_size_room));
      }

      // todo: put the key some in already existing rooms

      // todo: create secret rooms

      // todo: add some treasure

      if (step == (level.level-1)) {
        continue;
      }
      let r = this.moveEnd(level, end, before_end);
      if (r == false) {
        return level; // we are blocked -
      }

      // Todo transform previous arrival into door / gate / other blocking stuff
      //level.addSpace(new Space([end], Game.trap)); // open for now

      end = r[0];
      before_end = r[1];


    }

    return level;
  }

  moveEnd(level, end, before_end)
  {
    const corridor_size = Game.getRandomIntInclusive(this.min_size_coridor, this.max_size_coridor);
    let direction = undefined;
    let counter = 0;

    while (direction == undefined &&
           ++counter < this.max_tries) {
      const direction = Game.getRandomOrientation();
      if (this.isFree(level, end, Game.getMoveFromDirection(direction), corridor_size, true)) {
        console.log(`Found ${direction} + ${corridor_size}`);
        const corridor = new Corridor(end, direction, corridor_size);
        level.addSpace(corridor);
        level.grid.setB(before_end, Game.open);
        const new_end = end.add(Game.getMoveFromDirection(direction), corridor_size-1);
        const new_before_end = end.add(Game.getMoveFromDirection(direction), corridor_size-2);
        level.grid.setB(new_end, Game.end);
        return [new_end, new_before_end];
      } else {
        console.log(`NOT Found ${direction} + ${corridor_size}`);
      }
    }
    return false;
  }

  addRoom(level, width, height) {
    const blocs = level.per_bloc_type[Game.bloc_limit];

    let direction = undefined;

    while (direction == undefined  &&
           blocs.length != 0 ) {
      // choose a bloc
      let bloc = blocs[Game.getRandomIntInclusive(0, blocs.length-1)];
      bloc = new Bloc(bloc[0], bloc[1]);
      // remove it from list of available bloc
      let index = blocs.indexOf(bloc);
      blocs.splice(index, 1);

      // Can we build something from here
      direction = this.getDirection(level, bloc);
      if (direction == undefined) {
        continue;
      }

      let move = Game.getMoveFromDirection(direction);
      let ortho_move = Game.getOrthogonalMoveFromDirection(direction);

      //check width or height in direction
      if (!this.isFree(level, bloc, move, width+2)) {
        direction = undefined;
        continue;
      }
      const first_bloc = bloc.add(move,1);
      const list_blocs = [first_bloc];

      let distance = 1, both_direction = true, is_blocked = false, current_bloc = first_bloc;
      while (list_blocs.length < height &&
             !is_blocked ) {
        // check in one direction:
        current_bloc = first_bloc.add(ortho_move, distance);
        if (this.isFree(level, current_bloc, move, width+2))  {
          list_blocs.push(current_bloc);
          if (both_direction) {
            ortho_move.scale(-1);
          } else {
            distance += 1;
          }
        } else { // no place
          if (!both_direction) {
            is_blocked = true;
            continue;
          } else {
            both_direction = false;
            ortho_move.scale(-1);
          }
        }
        if (list_blocs.length == height) {
          continue;
        }
        // Other direction -
        current_bloc = first_bloc.add(ortho_move, distance);
        if (this.isFree(level, current_bloc, move, width))  {
          list_blocs.push(current_bloc);
          distance += 1;
          if (both_direction) {
            ortho_move.scale(-1);
          }
        } else { // No place
          if (!both_direction) {
            is_blocked = true;
            continue;
          } else {
            both_direction = false;
            ortho_move.scale(-1);
            distance += 1;
          }
        }
      }
      if (list_blocs.length != height) {
        direction = undefined;
        continue;
      }
      const side = new Space(list_blocs);
      const room = Room.getRoomFromSide(side, direction, width);
      level.addSpace(room);
      // add access to the room - may be replaced by a simple door
      level.addSpace(new Space([bloc], Game.open));

      // change bloc around door as indestru
      return ;
    }
  }

  isFree(level, origin, move, size, jump=0, check_width = false) {
    let bloc = origin;
    const ortho_move = move.turn();
    let bloc_1 = bloc.add(ortho_move);
    ortho_move.scale(-1);
    let bloc_2 = bloc.add(ortho_move);
    if (jump >0) {
      bloc = bloc.add(move, jump);
      bloc_1 = bloc_1.add(move, jump);
      bloc_2 = bloc_2.add(move, jump);
    }
    for (let i = 1; i<= size- jump; i++) {
      bloc = bloc.add(move, 1);
      if (level.grid.getB(bloc) !== undefined) {
        return false;
      }
      if (check_width) {
        bloc_1 = bloc_1.add(move, 1);
        if (level.grid.getB(bloc_1) !== undefined) {
          return false;
        }
        bloc_2 = bloc_2.add(move, 1);
        if (level.grid.getB(bloc_2) !== undefined) {
          return false;
        }
      }

    }
    return true;
  }

  // Caution: do not -yet- detect corner!
  getDirection(level, bloc) {
    const n = level.grid.get(bloc.x,bloc.y+1),
          e = level.grid.get(bloc.x+1,bloc.y),
          s = level.grid.get(bloc.x,bloc.y-1),
          w = level.grid.get(bloc.x-1,bloc.y);
    if (s == Game.open &&
        n == undefined)
    {
      return Game.north;
    }
    if (w == Game.open &&
        e == undefined)
    {
      return Game.east;
    }
    if (n == Game.open &&
        s == undefined)
    {
      return Game.south;
    }
    if (e == Game.open &&
        w == undefined)
    {
      return Game.west;
    }
    return undefined;
  }
}


class Level extends Space {

  level = undefined;
  spaces = [];
  per_bloc_type = {};
  arrival = undefined;
  //end = undefined;

  constructor(level=1) {
    super();
    this.spaces = [];
    this.level = level;
  }

  addSpace(space) {
    this.spaces.push(space);
    this.updateMinMax(space);
    this.addToGrid(space);
  }

  // Move that in grid?
  addToGrid(space) {
    space.grid.arr.forEach( (value, index) => {
      this.grid.arr[index] = value;
      if (this.per_bloc_type[value[0]] == undefined) {
        this.per_bloc_type[value[0]] = [];
      }
      this.per_bloc_type[value[0]].push([value[1], value[2]]);
    });
  }
}


export { Game, Space, Corridor, Room, Blocking, Gate, LevelConstructor, Level, Grid }
