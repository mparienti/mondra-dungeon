
import { Game, LevelConstructor,  } from "/mondra-dungeon/js/dungeon.js";

const $ = (sel) => document.querySelector(sel);

const laby = [[1,1,1,1,1,1,1],
        [0,0,1,0,0,0,1],
        [1,0,1,0,1,0,1],
        [1,0,0,0,1,0,1],
        [1,1,0,1,0,0,1],
        [1,1,0,0,1,1,1],
        [1,0,1,0,0,0,1],
        [1,0,1,1,1,0,1]
       ];

/*
  Variables used by old code for labyrinth
*/
const entry = [1,0];
const orientation = 'E';
const dim = 4;
const factor = 1.5;

const build_maze = (maze, dimension) => {
  for(let line in maze) {
    for (let col in maze[line]) {
      let val = maze[line][col];
      switch(val){
      case Game.open:
      case Game.end:
        create_allow(line, col, dimension, val);
        break;
      case Game.arrival:
        create_allow(line, col, dimension, val);
        move_to_entry([line,col], dim);
        break;
      default:
        add_block(line, col, dimension, val);
        break;
      }
    }
  }
};

const add_block = (i, j, dim, value) => {
  const block = document.createElement('a-box');
  const h = factor*dim;
  block.setAttribute('width', dim);
  block.setAttribute('height', h);
  block.setAttribute('depth', dim);
  block.setAttribute('static-body', "shape:box");
  block.setAttribute('material', `src:#wall1; repeat: 1 ${factor}`);

  block.setAttribute('position', i*dim + ' ' + h/2 + ' ' + -1*j*dim );

  $('#scene').appendChild(block);
}

const create_allow = (i, j, dim, val) => {
  const ground = document.createElement('a-plane');
  ground.setAttribute('width', dim);
  ground.setAttribute('height', dim);
  ground.setAttribute('rotation', "-90 0 0");
  ground.setAttribute('static-body', "");

  ground.setAttribute('position', i*dim + ' 0 ' + -1*j*dim );
  switch (val) {
  case Game.end:
    ground.setAttribute('material', `src:#end;`);
    break;
  case Game.arrival:
    ground.setAttribute('material', `src:#arrival;`);
    break;
  default:
    ground.setAttribute('material', `src:#sol2;`);
  }
  ground.setAttribute('id', `g_x_${i}_y${j})`);
  //ground.setAttribute('nav-mesh', '');
  $('#scene').appendChild(ground);

  const ceil = document.createElement('a-plane');
  ceil.setAttribute('width', dim);
  ceil.setAttribute('height', dim);
  ceil.setAttribute('rotation', "90 0 0");
  ceil.setAttribute('static-body', "");
  const h = factor*dim;
  ceil.setAttribute('position', i*dim + ' '+ h + ' ' + -1*j*dim );
  ceil.setAttribute('material', `src:#ceil1;`);
  ceil.setAttribute('id', `s_x_${i}_y${j})`);

  $('#scene').appendChild(ceil);

};

const move_to_entry = (e, dim) => {
  $('#rig').setAttribute('position', `${e[0]*dim} 1.6 ${-1*e[1]*dim}` );
};


function on_load() {

  const lc = new LevelConstructor();
  lc.min_room_pro_step = 3;
  lc.max_room_pro_step = 6;
  lc.min_size_room = 2;
  lc.max_size_room = 5;
  lc.min_size_coridor = 5;
  lc.max_size_coridor = 8;
  lc.max_tries = 16;

  const dungeon = lc.build(6);

  console.log(dungeon.toString());
  build_maze(dungeon.export(), dim);

  $('#rig').setAttribute('kinematic-body', '');

};

document.addEventListener("DOMContentLoaded", on_load);
