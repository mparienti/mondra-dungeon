*History*

The code to generate a maze from an array was written in 2017, and slept on my hard drive since. I read the challenge at the beginning of the month and decided to use this old code with an new algorithm to generate an awesome dungeon.

Sadly I didn't manage to complete my plans, and at this moment (October the 21th) you can only see and play a WIP, very buggy, annoying to play and laughable.

*WarningS*

This work is far from done, and far from what I want to achieve. For now it's just random rooms, and you have to go from point A (a bloc with wood at the ground) to point E (a bloc with a green ground). My plan initial plan was to add some keys needed to open some door, and the goal of the algorithm was to put those keys only in room you can reach (not behind the door clocked by those keys). Without the keys, the dungeon lost most of its interest. Moreover the departure and the arrival are awfully to close from each other.

The code source is absolute ugly and unprofessional. It needs a complete refactoring.

I used the AFrame framework; so theoretically it should work with VR devices. BUT I use the obsolete `kinematic-body` plugin; so using a VR devices for this soft should be avoided.


Textures are provided by:
1. Texturehaven.com (the ground)
2. Gametextures.com (all the others)

